# C++ Smart Pointer Implementation
This is the repo for learning modern C++ features.

One of the prominent feature for C++11 is smart pointers, including `unique_ptr` and `shared_ptr`,
which can date back to C++98 with a implementation based on imperfect language support. For example, [SRombauts](https://github.com/SRombauts/shared_ptr).

With the feature `rvalue reference` introduced in C++11 specifications, these smart pointers
can be implemented with full language support, thus resulting in pattern shift from [Rule of Three](https://en.wikipedia.org/wiki/Rule_of_three_\(C%2B%2B_programming\)) to
[Rule of Five](https://en.wikipedia.org/wiki/Rule_of_three_\(C%2B%2B_programming\)).

This repo is a curious inquiry into the C++11 features, even C++14/17, as to learn from practices
and home-brewing implementations for basic C++ classes.

# Structure of this project
This project will be composed of 2 parts,

1. The first part is intended to provide full implementations for popular smart pointers, namely `unique_ptr` and `shared_ptr`.
2. The second part is to provide usage examples for these smart pointers, as tests and code snippets for smart pointers.

# TODOs

1. Use Google Test lib for unit tests
