CC=g++
STDFLAGS?=c++11
CFLAGS=-g

INCLUDES?=src/include
DEFINES?=-D"ENABLE_COPY_AND_SWAP" -U"SHOW_MEMORY_OPS"

TEST_ROOT?=src/tests
UNIQUE_PTR_TEST=unique_ptr_test
SHARED_PTR_TEST=shared_ptr_test

%.o: %.cpp
	$(CC) -std=$(STDFLAGS) $(DEFINES) $(CFLAGS) -I$(INCLUDES) -c $^ -o $@

$(UNIQUE_PTR_TEST): $(TEST_ROOT)/$(UNIQUE_PTR_TEST).o
	$(CC) -std=$(STDFLAGS) $(CFLAGS) $^ -o build/$@ $(LINKS)

$(SHARED_PTR_TEST): $(TEST_ROOT)/$(SHARED_PTR_TEST).o
	$(CC) -std=$(STDFLAGS) $(CFLAGS) $^ -o build/$@ $(LINKS)

test: $(UNIQUE_PTR_TEST) $(SHARED_PTR_TEST)
	@echo "======================="
	@echo "|testing unique_ptr...|"
	@echo "======================="
	@./build/$(UNIQUE_PTR_TEST)
	@echo "======================="
	@echo "|testing shared_ptr...|"
	@echo "======================="
	@./build/$(SHARED_PTR_TEST)

clean:
	rm ./build/* || true
	rm $(TEST_ROOT)/*.o || true
