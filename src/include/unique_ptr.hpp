#ifndef UNIQUE_PTR
#define UNIQUE_PTR

#include <iostream>
#include <algorithm>

namespace smart_ptr {
  template <class T>
  class unique_ptr {
  private:
    T *ptr = nullptr;

    // internal swap function
    void swap(unique_ptr &src) noexcept {
      std::swap(ptr, src.ptr);
    }

  public:
    // Empty Contructor
    unique_ptr(void) : ptr(nullptr) {}

    // Raw pointer Contructor
    unique_ptr(T *p) : ptr(p) {}

    void reset() noexcept {
#ifdef SHOW_MEMORY_OPS
      std::cout << "-> unique_ptr released." << std::endl;
#endif  // SHOW_MEMORY_OPS
      delete ptr;
      ptr = nullptr;
    }

    // Destructor, 1/5
    ~unique_ptr() {
#ifdef SHOW_MEMORY_OPS
      std::cout << "-> unique_ptr::destructor..." << std::endl;
#endif  // SHOW_MEMORY_OPS
      reset();
    }

    // Copy Constructor, 2/5
    // Disable unique_ptr copy from lvalue
    unique_ptr(const unique_ptr &src) = delete;

    // Move Constructor, 3/5
    // This should only move(steal) from src
    // And resources swapped into src, will be released when out of this function
    unique_ptr(unique_ptr &&src) {
#ifdef SHOW_MEMORY_OPS
      std::cout << "-> unique_ptr::move contructor..." << std::endl;
#endif  // SHOW_MEMORY_OPS
      swap(src);
    }

    // Copy Assignment Contructor, 4/5
    // For unique_ptr, assignments are disabled
    unique_ptr& operator=(const unique_ptr &src) = delete;

    // Move Assignment Constructor, 5/5
    // For unique_ptr, assignments are disabled
    unique_ptr& operator=(unique_ptr &&src) = delete;

    bool operator==(const unique_ptr &b) {
      return (ptr == b.ptr);
    }

    bool operator!=(const unique_ptr &b) {
      return !(*this == b);
    }

    // * operator
    T& operator*() {
      return *ptr;
    }

    // -> operator
    T* operator->() noexcept {
      return ptr;
    }

  };
}

#endif  // UNIQUE_PTR
