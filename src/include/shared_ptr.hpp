#ifndef SHARED_PTR
#define SHARED_PTR

#include <iostream>
#include <algorithm>

namespace smart_ptr {
  class shared_reference_count {
  public:
    long cnt;

    shared_reference_count() : cnt(1) {}

    bool no_reference() {
      return cnt <= 0;
    }

    long references() {
      return cnt;
    }

    shared_reference_count(const shared_reference_count &src) = delete;
    shared_reference_count(shared_reference_count &&src) = delete;

    shared_reference_count& operator=(const shared_reference_count &src) = delete;
    shared_reference_count& operator=(shared_reference_count &&src) = delete;
  };

  template <class T>
  class shared_ptr {
  private:
    T *ptr = nullptr;
    shared_reference_count *cnt = nullptr;

    void swap(shared_ptr &src) {
      std::swap(ptr, src.ptr);
      std::swap(cnt, src.cnt);
    }

  public:
    shared_ptr() {}

    // Destructor, 1/5
    ~shared_ptr() {
#ifdef SHOW_MEMORY_OPS
      std::cout << "-> shared_ptr::destructor..." << std::endl;
#endif  // SHOW_MEMORY_OPS
      reset();
    }

    void reset() {
      if (cnt && --(cnt->cnt) == 0) {
#ifdef SHOW_MEMORY_OPS
        std::cout << "-> shared_ptr released." << std::endl;
#endif  // SHOW_MEMORY_OPS
        delete ptr;
        delete cnt;
      }
      ptr = nullptr;
      cnt = nullptr;
    }

    shared_ptr(T *src_ptr) : ptr(src_ptr) {
      // If src_ptr is nullptr, we don't create reference count
      // There is no point maintaining shared_ptr to nullptr
      if (src_ptr) cnt = new shared_reference_count();
    }

    // Copy Constructor, 2/5
    shared_ptr(const shared_ptr &src) {
#ifdef SHOW_MEMORY_OPS
      std::cout << "-> shared_ptr::copy constructor..." << std::endl;
#endif  // SHOW_MEMORY_OPS
      ptr = src.ptr;
      cnt = src.cnt;

      if (cnt) ++(cnt->cnt);
    }

    // Move Constructor, 3/5
    // Secretly steal from src.
    shared_ptr(shared_ptr &&src) {
#ifdef SHOW_MEMORY_OPS
      std::cout << "-> shared_ptr::move constructor..." << std::endl;
#endif  // SHOW_MEMORY_OPS
      ptr = src.ptr;
      cnt = src.cnt;
    }

#ifndef ENABLE_COPY_AND_SWAP
    // Copy Assignment Operator, 4/5
    shared_ptr& operator=(const shared_ptr &src) {
#ifdef SHOW_MEMORY_OPS
      std::cout << "-> shared_ptr::copy assignment..." << std::endl;
#endif  // SHOW_MEMORY_OPS
      if (*this != src) {
        reset();
        ptr = src.ptr;
        cnt = src.cnt;

        if (cnt) ++(cnt->cnt);
      }
      return *this;
    }

    // Move Assignment Operator, 5/5
    shared_ptr& operator=(shared_ptr &&src) {
#ifdef SHOW_MEMORY_OPS
      std::cout << "-> shared_ptr::move assignment..." << std::endl;
#endif  // SHOW_MEMORY_OPS
      if (*this != src) {
        reset();
        swap(src);
      }
      return *this;
    }
#else
    // Copy Assignment and Move Assignment Operators in one. 4/5, 5/5
    // Notice the param `src` type and implicit conversion it uses (copy constructor).
    shared_ptr& operator=(shared_ptr src) {
#ifdef SHOW_MEMORY_OPS
      std::cout << "-> shared_ptr::copy/move assignment..." << std::endl;
#endif  // SHOW_MEMORY_OPS
      swap(src);
      return *this;
    }
#endif  // ENABLE_COPY_AND_SWAP

    long references() {
      if (cnt) return cnt->cnt;
      return 0;
    }

    bool operator==(const shared_ptr &b) {
      return (ptr == b.ptr && cnt == b.cnt);
    }

    bool operator!=(const shared_ptr &b) {
      return !(*this == b);
    }

    // * operator
    T& operator*() {
      return *ptr;
    }

    // -> operator
    T* operator->() noexcept {
      return ptr;
    }
  };
}

#endif  // SHARED_PTR
