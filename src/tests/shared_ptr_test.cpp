#include "shared_ptr.hpp"
#include <utility>
#include <iostream>

using namespace std;

void test_func() {
  cout << "+ Create a_ptr with new int(23)" << endl;
  smart_ptr::shared_ptr<int> a_ptr(new int(23));
  cout << "a_ptr = " << *a_ptr << endl;
  cout << "++(*a_ptr) = " << ++(*a_ptr) << endl;

  cout << "+ Init b_ptr with b_ptr(a_ptr)" << endl;
  smart_ptr::shared_ptr<int> b_ptr(a_ptr);
  if (a_ptr == b_ptr) {
    cout << "a_ptr == b_ptr" << endl;
  }
  cout << "a_ptr.references() = " << a_ptr.references() << endl;
  if (a_ptr != nullptr) {
    cout << "a_ptr is not nullptr." << endl;
  }
  cout << "b_ptr.references() = " << b_ptr.references() << endl;
  cout << "b_ptr = " << *b_ptr << endl;
  cout << "++(*b_ptr) = " << ++(*b_ptr) << endl;
  cout << "a_ptr = " << *a_ptr << endl;

  cout << "+ Create c_ptr" << endl;
  smart_ptr::shared_ptr<int> c_ptr;
  if (c_ptr != b_ptr) {
    cout << "c_ptr != b_ptr" << endl;
  }
  cout << "c_ptr.references() = " << c_ptr.references() << endl;

  cout << "+ Assign c_ptr = b_ptr" << endl;
  c_ptr = b_ptr;
  if (c_ptr == b_ptr) {
    cout << "c_ptr == b_ptr now" << endl;
  }
  cout << "c_ptr.references() = " << c_ptr.references() << endl;
}

int main() {
  test_func();
}
