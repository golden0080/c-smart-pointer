#include "unique_ptr.hpp"
#include <utility>
#include <iostream>

using namespace std;

void test_func() {
  cout << "+ Create a_ptr with new int(23)" << endl;
  smart_ptr::unique_ptr<int> a_ptr(new int(23));
  cout << "a_ptr = " << *a_ptr << endl;
  cout << "++(*a_ptr) = " << ++(*a_ptr) << endl;

  cout << "+ Init b_ptr with b_ptr(std::move(a_ptr))" << endl;
  smart_ptr::unique_ptr<int> b_ptr(std::move(a_ptr));
  if (a_ptr == nullptr) {
    cout << "a_ptr is nullptr now." << endl;
  }
  cout << "b_ptr = " << *b_ptr << endl;
}

int main() {
  test_func();
}
